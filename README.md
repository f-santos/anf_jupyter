# Mon dépôt personnel pour l'ANF

Je placerai ici régulièrement mes documents produits durant l'ANF.

Les analyses seront conduites en utilisant R 4.1.0 et disponibles sous formes de *notebooks* créés avec JupyterLab.
